from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
# 
#

class LocationVO(models.Model):
    shelf_number = models.PositiveSmallIntegerField(null=True)
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=200, unique=True)



# Create a model for hats:
# CLASS HAT : 
#  - fabric, - style name, - color, - a URL for a picture,
# and - the location
class Hat(models.Model):
     fabric = models.CharField(max_length = 200)
     style_name = models.CharField(max_length = 200)
     color = models.CharField(max_length = 200)
     picture_url = models.URLField(null=True)
     location = models.ForeignKey(
          LocationVO,
          related_name = "hats",
          on_delete=models.CASCADE
     )

     def __str__(self):
        return self.style_name

     def get_api_url(self):
         return reverse("api_hat", kwargs={"pk": self.pk})