from django.shortcuts import render
from django.http import JsonResponse #
from django.views.decorators.http import require_http_methods #

import json #
from common.json import ModelEncoder #
from .models import Hat, LocationVO #

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "shelf_number",
        "closet_name",
        "section_number",
        "import_href",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]

    # def get_extra_data(self, o):
    #     return {"location": o.location.name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
        "shelf_number",
        "section_number",
    ]

# CREATE A VIEWS FOR  LIST Of HATS
@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()    

        return JsonResponse({
            "hats": hats
        },
            encoder = HatDetailEncoder,
        )
    else: # CREATING HAT
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content["location"]) # look at the models.py for locationvo
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
    )     

@require_http_methods({"GET", "DELETE", "PUT"})
def api_show_hats(request,pk):
  
    if request.method == "GET":
        try:
            count, _ = Hat.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hats = Hat.objects.get(id=pk)
            hats.delete()
            return JsonResponse(
                hats,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: #### 
        try:
            content = json.loads(request.body)
            hats = Hat.objects.get(id=pk)

            props = ["fabric", "style_name", "color", "picture_url", "location", "id"]
            for prop in props:
                if prop in content:
                    setattr(hats, prop, content[prop])
            hats.save()
            return JsonResponse(
                hats,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
        return response


