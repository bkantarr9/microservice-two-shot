## NEED TO ADD URLS>PY UNDER HATS_REST- CREATE URL

from django.urls import path
from .views import api_show_hats, api_list_hats


urlpatterns = [
    path("hats/<int:pk>/", api_show_hats, name="api_show_hat"),
    path("hats/", api_list_hats, name="api_list_hats"),
]