import React, {useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';

function CreateShoesForm() {
    const [bins, setBins] = useState([]); // New Bin for storing Bins data
    const [name, setName] = useState(''); // name for storing name input value
    const [manufacturer, setManufacturer] = useState(''); // manufacturer for storing room count input value
    const [color, setColor] = useState(''); // Color for storing city input value
    const [picture, setPicture] = useState(''); // Picture for storing picture input valueset
    const [selectedBins, setSelectedBins] = useState(''); // storing selected Bins

    const navigate = useNavigate();
    // Fetch states data from API
    const fetchData = async () => {
        try {
            const url = 'http://localhost:8100/api/bins/';
            const response = await fetch(url);
            //if the connection is success
            if (response.ok) {
                const data = await response.json();
                setBins(data.bins);                
            } else {
                throw new Error('Failed to fetch Bin');
            }
        } catch (error) {
            console.error('Error fetching Bin:', error);
        }
    };

    useEffect(() => {
        fetchData();// Fetch Bins data when component mounts
    }, []);

    // Created handleRoomCountChange method & store it in the state's "roomcount" variable 
    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    };
    // Created handleNameChange method & store it in the state's "name" variable   
    const handleNameChange = (event) => {
        setName(event.target.value);
    };
    // Created handleCityChange method & store it in the state's "city" variable 
    const handleColorChange = (event) => {
        setColor(event.target.value);
    };
        // Created handleCityChange method & store it in the state's "city" variable 
    const handlePictureChange = (event) => {
        setPicture(event.target.value);
    };
    // Created handleStateChange method & store it in the state's "selectedState" variable 
    const handleBinsChange = (event) => {
        setSelectedBins(event.target.value);
    };

    // Handle form submission
    const handleSubmit = async (event) => {
        event.preventDefault();  
    // Create data object with form values    
    const data = { manufacturer, model_name: name, color, picture, bin: selectedBins };
    //POST code from new-Shoes.js
    const shoesUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
  
    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      const Shoes = await response.json();
      alert("Message, Shoe was created!");
      navigate(-1);
      console.log(Shoes);      
      setManufacturer('');
      setName('');           
      setColor('');
      setPicture('');
      setSelectedBins('');      
    }else{
        alert("Message, Shoe was not created!");
    }

};  
 // Return JSX ...   
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Shoes</h1>
                    <form onSubmit={handleSubmit} id="create-Shoes-form">                     
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} placeholder="manufacturer" required 
                                type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required
                                type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required
                                type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} placeholder="Picture" required
                                type="text" name="picture" id="picture" className="form-control" />
                            <label htmlFor="picture">picture</label>
                        </div>
                        <div className="mb-3">            
                            <select onChange={handleBinsChange} required name="bins" id="bins" className="form-select">
                                <option value="">Choose a Bin</option>
                                {bins.map(bin => (                
                                    <option key={bin.id} value={bin.href}>
                                        {bin.closet_name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateShoesForm;