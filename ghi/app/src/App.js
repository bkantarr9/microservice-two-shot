import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CreateShoesForm from './CreateShoesForm';
import ShoesList from './ShoesList';
import MainPage from './MainPage'
import Nav from './Nav'
import HatsForm from './HatsForm'
import HatsList from './HatsList'

import React from 'react'


function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes/new" element={<CreateShoesForm/>} />
          <Route path="/shoes/" element={<ShoesList />} />  
          <Route path="hats">
              <Route index element={<HatsList />} />
              <Route path="new" element={<HatsForm />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
