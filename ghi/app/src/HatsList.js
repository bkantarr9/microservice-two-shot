import React from 'react';
import { useEffect, useState } from 'react';

function HatsList() {
  const [hats, setHats] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/hats/');
    
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  }

  // DELETE BUTTON ON HAT LIST-BROWSER
const deleteHat = async function(hatToDelete) {
  const response = await fetch (`http://localhost:8090/api/hats/${hatToDelete}/`, {
    method: "DELETE",
  });
  if (response.ok) {
    getData()
  } else {
    console.log(response)
  }
}
// DELETE Button  ADDED LINE 47 FOR BROWSER-
  useEffect(()=>{
    getData()
  }, [])

    return (
        <table className="table table-striped table-secondary">
          <thead class="table-info">
            <tr class="table-active">
              <th>Delete</th>
              <th>Fabric</th>
              <th>Style / Name</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Location</th>
            </tr>
          </thead>
          <tbody>
            {hats.map(hat => {
              return (
                <tr key={hat.id}>
                  <td><div><button onClick={() => deleteHat(hat.id)}>Delete</button></div></td>    
                  <td>{ hat.fabric }</td>
                  <td>{ hat.style_name }</td>
                  <td>{ hat.color }</td>
                  <td>{ hat.picture_url }</td>
                  <td>{ hat.location.closet_name }</td> 
                  <td><div><img className = "img-fluid img-thumbnail" src={hat.picture_url} alt={hat.style_name} style={{width:'100px', height: '100px'}}  /></div></td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    }
    
    export default HatsList;


// only you can pull import_href    
    
