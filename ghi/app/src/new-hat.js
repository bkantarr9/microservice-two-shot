window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/hats/';

    const response = await fetch(url);

    if(response.ok){
        const data = await response.json();

        selectTag = document.getElementById('location');
        for (let location of data.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.hat;
            selectTag.appendChild(option);
        }
    }

    const formTag = document.getElementById('create-hat-form');
    const selectTag = document.getElementById('location');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const FormData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(FormData));

        const locationId = selectTag.options[selectTag.selectedIndex].value;
        const locationUrl = `http://localhost:8000/api/locations/${locationId}/hat`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newHat = await response.json();
            console.log(newHat);
        }
    })
//  conference = location   conferenceId= locationid  newconference=newhat
