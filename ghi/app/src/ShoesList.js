import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

function ShoesList() {
    const [shoeList, setShoes] = useState([]);    
    // getting the shoes list
    const fetchData = async () => {
        try {
            const url = 'http://localhost:8080/api/shoes/';
            const response = await fetch(url);            
            if (response.ok) {//if the connection is success
                const data = await response.json();
                setShoes(data.shoes);
            } else {
                throw new Error('Failed to fetch Shoes');
            }
        } catch (error) {
            console.error('Error fetching Shoes:', error);
        }
    };

    const handleDelete= async (idx) => {
        try {// DELETE method with the shoe id            
            const deletedShoeId = shoeList[idx].id;
            const url = `http://localhost:8080/api/shoes/${deletedShoeId}`;
            const response = await fetch(url, { method: 'DELETE' });

            if (response.ok) {
                // Remove the deleted shoe from the shoeList
                setShoes(prevShoes => prevShoes.filter((_, index) => index !== idx));
            } else {
                throw new Error('Failed to delete Shoe');
            }
        } catch (error) {
            console.error('Error deleting Shoe:', error);
        }
    };

    useEffect(() => {
        fetchData();// Fetch Shoes data when component mounts for one time
            return () =>{} //clean up function
    }, []);

    return (
        <div className="shoes-List">  
            <Link to="/shoes/new">Create New</Link>  
            <div className="Shoes-Detail">
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <th>Manufacture</th>
                        <th>Model Name</th>
                        <th>Picture</th>
                        <th>Shoes id</th>
                        <th>Bin Number</th>
                        </tr>
                    </thead>
                <tbody>
                    {shoeList.map((shoe, idx) => {
                        return (
                                <tr key={shoe.id}>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.model_name }</td>
                                <td>{ shoe.color }</td>
                                <td style={{width:"100px", height:"100px"}} ><img src={shoe.picture} style={{width:"100%"}}/></td>
                                <td>{ shoe.bin.binNumber }</td>
                                <td><button onClick={() => handleDelete(idx)} type="button">Delete</button></td>
                                </tr>                                
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
    export default ShoesList;
