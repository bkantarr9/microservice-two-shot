import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Wardrobify</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item" >
              <NavLink className="nav-link" to="/shoes">Shoes</NavLink>
            </li>
            <li className="nav-item dropend">
            <NavLink button className="nav-link active" to="/hats" role="button" data-toggle="dropend" aria-haspopup="true" aria-expanded="false" >Hats</NavLink>
            <div className="btn-group dropend" aria-labelledby="hatDropdownMain">
            <button className="nav-item" >    
                <NavLink className="hat_dropdown" to="/hats/new">Add new Hat</NavLink>
            </button>    
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;


