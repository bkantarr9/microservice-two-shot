import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO

def get_bin_location():
    url= "http://wardrobe-api:8000/api/bins/"
    # url= "http://localhost:8000/api/bins/"
    response = requests.get(url)
    content= json.loads(response.content)
    for bins in content["bins"]:
        BinVO.objects.update_or_create(
            import_href = bins["href"],
            # creating the object from the model
            defaults = {              
                "closet_name": bins["closet_name"],
                "bin_number": bins["bin_number"], 
                "bin_size": bins["bin_size"],         
            }
        )

def poll():
    while True:
        print('Shoes poller polling for data')
        try:                       
            get_bin_location()  # retrieving the fuction above   
            print(BinVO.objects.all())
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
