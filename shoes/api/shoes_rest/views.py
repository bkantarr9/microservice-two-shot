from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoes, BinVO
from common.json import ModelEncoder
import json

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        # "id",
        "closet_name",        
        "bin_number",
        "bin_size",
        "import_href",
    ]

# Defines a custom JSON encoder for the BinVO model with specific properties.
class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["import_href"]
        #[ "shoe_id",
        # "manufacturer",
        # "model_name",        
        # "color",
        # "picture_url",
        # "bin",]   

# Defines a custom JSON encoder for the Shoes model with specific properties for listing.
# show the list  from the table database
class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [ 
        "id",       
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
    ]
    encoders = {
        "bin" : BinVODetailEncoder(),
    }

# Shoes list and creation endpoint
# shoes identity
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoesFilter = Shoes.objects.all()
        # shoesFilter = Shoes.objects.get(id=id)
        # serialize the shoe objects into JSON format
        return JsonResponse(
            {"shoes": shoesFilter},
            encoder = ShoesDetailEncoder
        )
    else: #using the POST to create        
        content = json.loads(request.body)# Parse the request body into a Python dict        
        try: 
             # Get the Bin object and put it in the content dict                    
            binObject = BinVO.objects.get(import_href=content["bin"])    
            content["bin"]= binObject           
        except BinVO.DoesNotExist:
            # Return a JSON response with an error message if the BinVO object does not exist
            return JsonResponse(
                {"message": "Invalid Bin ID"},
                status=400,
            )    
        # create a new instance of the model and save it to the database
        shoesObject = Shoes.objects.create(**content)
         # Return a JSON response with created Shoes object serialized into JSON format
        return JsonResponse(
            shoesObject,
            encoder = ShoesDetailEncoder,
            safe=False,
        )
    
    
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request,pk):
    if request.method == "GET":
        shoesObject = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoesObject,
            encoder = ShoesDetailEncoder,
            safe = False,
    )
    elif request.method == "DELETE":       
        try:               
            shoeObject = Shoes.objects.get(id=pk).delete()
            return JsonResponse(
                shoeObject,
                encoder=ShoesDetailEncoder,
                safe=False,
            )
        except shoeObject.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
 
    else:# Update method 
        content = json.loads(request.body)
        try:
            bin_loc = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin_loc            
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin"},
                status=400,
            )
        shoesObject = Shoes.objects.create(**content)
        return JsonResponse(
            shoesObject,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
     