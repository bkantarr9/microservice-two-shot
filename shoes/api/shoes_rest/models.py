from django.db import models
from django.urls import reverse
# Create your models here.class Shoe(models.Model):


# this is the Value Objects
class BinVO(models.Model):    
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)
    
class Shoes(models.Model):
    # shoe_id = models.PositiveIntegerField(primary_key=True)
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    picture = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name= "bin",
        # related_name= "shoes",
        on_delete=models.CASCADE
    )  

    # def __str__(self):
    #     return self.shoe_id
    
    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"id": self.id})
    
