from django.urls import path, include
# from . import views
from .views import (
    api_list_shoes,    
    api_show_shoes,      
)

urlpatterns = [
    path('shoes/', api_list_shoes, name='api_list_shoes'),
    path('shoes/<int:pk>/', api_show_shoes, name='api_show_shoes'),
    # path('bins/<int:pk>/', api_show_shoes, name='api_show_shoes'),
    # path('shoe/create/', api_create_shoe, name='api_create_shoe'),
    # path('shoe/delete/<int:shoe_id>/', api_delete_shoe, name='api_delete_shoe'),
]